##!/bin/bash

set -e

export WRKDIR=/work/cross
mkdir -p $WRKDIR
cd $WRKDIR

# Download binutils
wget http://ftpmirror.gnu.org/binutils/binutils-2.37.tar.gz
tar xf binutils-2.37.tar.gz

# Download mpc
wget ftp://gcc.gnu.org/pub/gcc/infrastructure/mpc-1.0.3.tar.gz
tar xvf mpc-1.0.3.tar.gz

# Download gcc
wget http://ftpmirror.gnu.org/gcc/gcc-11.1.0/gcc-11.1.0.tar.gz
tar xf gcc-11.1.0.tar.gz

# Download gdb
wget http://ftp.task.gda.pl/pub/gnu/gdb/gdb-11.1.tar.gz
tar xf gdb-11.1.tar.gz
