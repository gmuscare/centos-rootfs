# centos-rootfs
[![pipeline status](https://gitlab.cern.ch/soc/centos-rootfs/badges/master/pipeline.svg)](https://gitlab.cern.ch/soc/centos-rootfs/-/commits/master)

## mkrootfs.py

    Usage: mkrootfs.py [OPTIONS] COMMAND [ARGS]...

      Configures CentOS root file system

    Options:
      -r, --root PATH               Directory of rootfs, by default uses $SYSROOT
                                    from env  [required]
      -a, --arch [aarch64|armv7hl]  Target architecture, by default uses $ARCH
                                    from env  [required]
      -v, --verbose                 Verbose output
      --help                        Show this message and exit.

    Commands:
      groupinstall  Setups minimal rootfs installation
      install       Installs rpm packages from a file
      remove        Removes packages provided as arguments

## Docker image

The following env variables are set inside the docker image (example values for aarch64):

    CROSS_GXX=/arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/bin/aarch64-linux-gnu-g++
    CROSS_GCC=/arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc
    CROSS_VERSION=gcc11-aarch64-linux-gnu
    CROSS_DIR=/arm/compiler/gcc11-aarch64-linux-gnu
    CENTOS_VERSION=centos7
    SYSROOT=/arm/sysroot
    TOOLCHAIN=/arm/aarch64-toolchain.cmake
    TARGET=aarch64-linux-gnu
    ARCH=aarch64
    BINARY_TAG=aarch64-centos7-gcc11-opt
