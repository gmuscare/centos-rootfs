#!/bin/env python3
###############################################################
# Script based on original script written by Matthias Wittgen #
###############################################################

import logging
import os
import subprocess
import sys

import click


epels = {
    'aarch64': 'arm64-epel',
    'armv7hl': 'arm-epel'
}


def run_dnf(dnf_conf: str, rootfs: str, arch: str, command: str,
            packages: list):
    try:
        epel = epels[arch]
    except KeyError:
        print(f'Epel not defined for architecture: {arch}')
        exit(-1)

    cmd = ['dnf',
           '-y',
           '--skip-broken',
           '-c', dnf_conf,
           '--releasever=7',
           f'--forcearch={arch}',
           f'--repo=centos-base,centos-updates,centos-extras,base-debuginfo,{epel}',
           '--verbose',
           f'--installroot={rootfs}',
           command] + packages

    click.secho(f"{' '.join(cmd)}", fg='green', bold=True)

    retval = subprocess.call(cmd)
    if retval:
        exit(retval)


@click.group()
@click.option('-r', '--root',
              type=click.Path(exists=True),
              default=lambda: os.environ.get('SYSROOT', None),
              required=True,
              help='Directory of rootfs, by default uses $SYSROOT from env')
@click.option('-a', '--arch',
              type=click.Choice(['aarch64', 'armv7hl']),
              default=lambda: os.environ.get('ARCH', None),
              required=True,
              help='Target architecture, by default uses $ARCH from env')
@click.option('--dnf_conf',
              type=click.Path(exists=True),
              default='/arm/dnf.conf',
              help='Dnf configuration file')
@click.option('-v', '--verbose', is_flag=True, help='Verbose output')
@click.pass_context
def main(ctx, root, arch, dnf_conf, verbose):
    """
    Configures CentOS root file system
    """
    ctx.ensure_object(dict)

    if verbose:
        FORMAT = '%(levelname)s : %(message)s'
        logging.basicConfig(format=FORMAT, stream=sys.stdout,
                            level=logging.DEBUG)

    ctx.obj['root'] = root
    ctx.obj['arch'] = arch
    ctx.obj['dnf_conf'] = dnf_conf


@main.command()
@click.pass_context
def groupinstall(ctx):
    """
    Setups minimal rootfs installation
    """
    dnf_conf = ctx.obj['dnf_conf']
    run_dnf(dnf_conf, ctx.obj['root'], ctx.obj['arch'], "clean", ['all'])
    run_dnf(dnf_conf, ctx.obj['root'], ctx.obj['arch'], "update", [])
    run_dnf(dnf_conf, ctx.obj['root'], ctx.obj['arch'],
            "groupinstall", ['Minimal Install'])


@main.command()
@click.pass_context
@click.argument('packages_file', type=click.File('r'))
def install(ctx, packages_file):
    """
    Installs rpm packages from a file
    """
    packages = [p.replace("\n", "") for p in packages_file]
    run_dnf(ctx.obj['dnf_conf'], ctx.obj['root'], ctx.obj['arch'], "install",
            packages)


@main.command()
@click.pass_context
@click.argument('packages', nargs=-1)
def remove(ctx, packages):
    """
    Removes packages provided as arguments
    """
    run_dnf(ctx.obj['dnf_conf'], ctx.obj['root'], ctx.obj['arch'], "remove",
            list(packages))


@main.command()
@click.pass_context
@click.argument('name', nargs=1)
@click.argument('args', nargs=-1)
def command(ctx, name, args):
    """
    Runs a dnf command
    """
    run_dnf(ctx.obj['dnf_conf'], ctx.obj['root'], ctx.obj['arch'], name,
            list(args))


if __name__ == '__main__':
    if os.getuid() != 0:
        if not click.confirm('This program needs to run as superuser, do you want to continue?'):
            exit(-1)
        print(f"Relaunching: sudo {' '.join(sys.argv)}")
        os.execvp("sudo", [
                  "sudo",
                  f"PATH={os.getenv('PATH', '')}",
                  f"LD_LIBRARY_PATH={os.getenv('LD_LIBRARY_PATH', '')}",
                  f"PYTHONPATH={os.getenv('PYTHONPATH', '')}"] + sys.argv)
    sys.exit(main(obj={}))

